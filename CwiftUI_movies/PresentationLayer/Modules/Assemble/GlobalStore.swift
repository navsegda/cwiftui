//
//  Global Store.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 10.01.2021.
//

import Foundation

class GlobalStore {
    static let store = MoviesStore(reducer: reducer(state:action:))
    static let detailsStore = MovieDetailsStore(reducer: movieDetailsReducer(state:action:))
}
