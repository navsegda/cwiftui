//
//  Dot.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 22.11.2020.
//

import SwiftUI

struct OverHeadDot: Shape {
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: 10, height: 10))
        return Path(path.cgPath)
    }
}
