//
//  TabButton.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 22.11.2020.
//

import SwiftUI

struct TabButton : View {
    
    @State var matched: Bool = false
    @Binding var selectedTab : String
    var title : String
    var animation : Namespace.ID
    var dotSize: CGFloat = 5
    var gradient = LinearGradient(gradient: Gradient(colors: [.white, .black]), startPoint: .top, endPoint: .bottom)
    var body: some View{
        
        Button(action: {
            withAnimation{selectedTab = title}
            if selectedTab == "Новые" {
                GlobalStore.store.dispatch(action: ObtainFilmsAction(with: .nowPlaying, page: 1))
            } else if selectedTab == "Топ" {
                GlobalStore.store.dispatch(action: ObtainFilmsAction(with: .topRated, page: 1))
            } else if selectedTab == "Скоро" {
                GlobalStore.store.dispatch(action: ObtainFilmsAction(with: .popular, page: 1))
            }
        }) {
            ZStack {
                VStack(spacing: 6){

                        OverHeadDot()
                            .fill(Color.clear)
                            .frame(width: dotSize, height: dotSize)
                        if selectedTab == title{
                            OverHeadDot()
                                .fill(Color("tint"))
                                .frame(width: 20, height: dotSize)
                                .matchedGeometryEffect(id: "Tab_Change", in: animation)
                        }

                    Image(title)
                        .renderingMode(.template)
                        .resizable()
                        .foregroundColor(selectedTab == title ? Color("tint") : Color.black.opacity(0.4))
                        .frame(width: 24, height: 24)
                        .offset(x: 0, y: selectedTab == title ? 0 : 5)
                        .scaleEffect(selectedTab == title ? CGSize(width: 1.0, height: 1.0) : CGSize(width: 1.3, height: 1.3))
                    
                    Text(title)
                        .font(.caption)
                        .fontWeight(.bold)
                        .foregroundColor(Color("tint"))
                        .opacity(selectedTab == title ? 1 : 0)
                }
            }
            .padding(.horizontal, 40)
            .padding(.bottom, 20)
            .padding(.top, 10)
        }
    }
}
