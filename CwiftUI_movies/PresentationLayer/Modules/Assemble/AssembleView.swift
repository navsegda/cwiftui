//
//  AssembleView.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 21.11.2020.
//

import SwiftUI

struct AssembleView: View {
    
    // MARK: - Property wrappers
    @State var selectedTab = "Новые"
    @Namespace var animation
    @Namespace var matchedAnimation
    @State var show = false
    @State var selected: Movie = Movie(id: 1, title: "Loading", overview: "Overview", releaseDate: "", posterPath: "")
    
    // MARK: - Public properties
    var tabs = ["Новые", "Топ", "Скоро"]
    var body: some View {
        
    // MARK: - Body
        ZStack{
            ZStack {
                MoviesView(show: $show, selected: $selected, text: "Новые фильмы", animation: matchedAnimation)
                    .opacity(selectedTab == "Новые" ? 1 : 0)
                    .opacity(show == false ? 1 : 0)
                    .environmentObject(GlobalStore.store)
                    .onAppear {
                        GlobalStore.store.dispatch(action: ObtainFilmsAction(with: .nowPlaying, page: 1))
                    }
                
                MoviesView(show: $show, selected: $selected, text: "Топ фильмы", animation: matchedAnimation)
                    .opacity(selectedTab == "Топ" ? 1 : 0)
                    .opacity(show == false ? 1 : 0)
                    .environmentObject(GlobalStore.store)
                
                MoviesView(show: $show, selected: $selected, text: "Скоро", animation: matchedAnimation)
                    .opacity(selectedTab == "Скоро" ? 1 : 0)
                    .opacity(show == false ? 1 : 0)
                    .environmentObject(GlobalStore.store)
                if show {
                    MovieDetailsView(selected: $selected, show: $show, details: GlobalStore.detailsStore, matchedAnimation: matchedAnimation, movieId: selected.id)
                }
                
            }.overlay(
                VStack(spacing: 0) {
                    GeometryReader { _ in
                    }
                    /// Custom TabBar setup
                    ZStack{
                        HStack(spacing: 0) {
                            ForEach(tabs, id: \.self) { tab in
                                TabButton(selectedTab: $selectedTab, title: tab, animation: animation)
                                if tab != tabs.last {
                                    Spacer(minLength: 0)
                                }
                            }
                        }
                        .background(Blur())
                        .cornerRadius(30)
                        .padding(.bottom, 10)
                        .padding(.horizontal, 10)
                        
                    }
                    .opacity(show == false ? 1 : 0)
                })
            .ignoresSafeArea(.all, edges: .bottom)
            .background(Color.white.ignoresSafeArea(.all, edges: .all))
        }
    }
    
}

// MARK: - Preview
struct AssembleView_Previews: PreviewProvider {
    static var previews: some View {
        AssembleView()
    }
}
