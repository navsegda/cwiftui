//
//  CwiftUI_moviesApp.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 21.11.2020.
//

import SwiftUI
import CFoundation

@main
struct CwiftUI_moviesApp: App {
    var body: some Scene {
        WindowGroup {
            AssembleView()
        }
    }
}

struct CwiftUI_moviesApp_Previews: PreviewProvider {
    static var previews: some View {
        AssembleView()
    }
}
