//
//  MovieDetailsState.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 19.12.2020.
//

import Foundation

struct MovieDetailsState {
    var movieDetails: MovieDetails = MovieDetails(adult: true, backdropPath: "", belongsToCollection: nil, budget: 1, genres: [], homepage: "", id: 1, imdbID: "", overview: "", originalLanguage: "", originalTitle: "", popularity: 1, posterPath: "", productionCompanies: [], productionCountries: [], releaseDate: "", revenue: 1, runtime: 1, spokenLanguages: [], tagline: "", status: "", title: "", video: true, voteAverage: 1, voteCount: 1)
    var casts: Casts = Casts(id: 1, cast: [], crew: [])
    var trailer: Trailer = Trailer(id: 1, results: [Result(id: "", iso639_1: "", iso3166_1: "", key: "", name: "", site: "", size: 1, type: "")])
}
