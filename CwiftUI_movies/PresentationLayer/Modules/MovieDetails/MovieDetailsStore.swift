//
//  MovieDetailsStore.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 19.12.2020.
//

import SwiftUI

class MovieDetailsStore: ObservableObject {
    var reducer: MoviesDetailsReducer
    @Published private (set) var state: MovieDetailsState
    
    init(reducer: @escaping MoviesDetailsReducer, state: MovieDetailsState = MovieDetailsState()) {
        self.reducer = reducer
        self.state = state
    }
    func dispatch(action: Action) {
        DispatchQueue.main.async {
            self.state = self.reducer(self.state, action)
        }
    }
}
