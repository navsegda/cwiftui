//
//  MovieDetails.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 19.12.2020.
//

import SwiftUI
import struct Kingfisher.KFImage

struct MovieDetailsView: View {
    
    // MARK: - Property wrappers
    @Binding var selected: Movie
    @Binding var show: Bool
    @ObservedObject var details: MovieDetailsStore
    @Environment(\.openURL) var openURL
    
    // MARK: - Public properties
    var matchedAnimation: Namespace.ID
    var movieId: Int
    
    // MARK: - Private properties
    private let imageHeight: CGFloat = 500
    private let starHieght: CGFloat = 25
    
    // MARK: - Body
    var body: some View {
        ZStack(alignment: .topLeading) {
            ScrollView(.vertical, showsIndicators: false, content: {
                GeometryReader { reader in
                    KFImage(URL(string: "https://image.tmdb.org/t/p/w1280" + selected.posterPath!))
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .offset(y: -reader.frame(in: .global).minY)
                        .frame(width: UIScreen.main.bounds.width, height: abs(reader.frame(in: .global).minY + imageHeight))
                        .matchedGeometryEffect(id: selected.posterPath, in: matchedAnimation)
                        .onChange(of: reader.frame(in: .global).minY){ value in
                            if value > 130 {
                                withAnimation(.linear(duration: 0.2)){
                                    show = false
                                }
                            }
                        }
                }
                .frame(height: imageHeight)
                
                VStack(alignment: .leading, spacing: 17){
                    Text(selected.title)
                        .bold()
                        .font(.system(.largeTitle))
                        .foregroundColor(Color("tint"))
                    HStack(spacing: 10){
                        StarView(color: Color("tint"), raiting: details.state.movieDetails.voteAverage )
                        Text("Оценка из 10: \(Int(details.state.movieDetails.voteAverage ))")
                    }
                    Button(action: {
                        if details.state.trailer.results.count != 0 {
                            details.state.trailer.results.prefix(1).forEach {
                                openURL(URL(string: "https://www.youtube.com/watch?v=\($0.key)")!)
                                
                            }
                        }
                    }, label: {
                        Image(systemName: "play.rectangle")
                            .foregroundColor(Color("tint"))
                        Text("Посмотреть трейлер")
                            .foregroundColor(.label)
                    })
                    HStack{
                        ForEach(details.state.movieDetails.productionCompanies){ company in
                            if company.logoPath != nil {
                            KFImage(URL(string: "https://image.tmdb.org/t/p/w1280" + (company.logoPath ?? "")))
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(height: 25)
                            }
                        }
                    }
                    if let overview = details.state.movieDetails.overview {
                        Text("\(overview)")
                            .font(.system(.headline))
                            .foregroundColor(.label)
                    }
                    if let runtime = details.state.movieDetails.runtime {
                        Text("Продолжительность: \(runtime) мин.")
                            .foregroundColor(.label)
                    }
                    VStack(alignment: .leading){
                        Text("Жанр:")
                            .font(.system(.headline))
                        ForEach(details.state.movieDetails.genres) { genre in
                            Text(genre.name)
                                .foregroundColor(.label)
                        }
                    }
                    VStack(alignment: .leading){
                        Text("Актеры:")
                            .font(.system(.headline))
                        CastsView(casts: details.state.casts.cast)
                    }
                    VStack(alignment: .leading){
                        Text("Съемочная команда:")
                            .font(.system(.headline))
                        CastsView(casts: details.state.casts.crew)
                    }
                }
                .padding(.horizontal, 10)
                .padding(.vertical)
                .background(Color.white)
                .cornerRadius(15)
                .offset(y: -35)
            })
            Button(action: {
                withAnimation(.linear(duration: 0.2)){
                    show = false
                }
            }, label: {
                Image(systemName: "xmark.circle.fill")
                    .resizable()
                    .frame(width: 25, height: 25)
                    .foregroundColor(Color("tint"))
            })
            .padding(.leading, 10)
        }
    }
}

