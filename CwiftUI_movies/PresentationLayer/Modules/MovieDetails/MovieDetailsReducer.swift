//
//  MovieDetailsReducer.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 19.12.2020.
//

import SwiftUI

typealias MoviesDetailsReducer = (MovieDetailsState, Action) -> MovieDetailsState

func movieDetailsReducer(state: MovieDetailsState, action: Action) -> MovieDetailsState {
    var state = state
    switch action {
    case let action as SetMovieDetailsAction:
        state.movieDetails = action.movieDetails
    case let action as SetMovieCastsAction:
        state.casts = action.movieCasts
    case let action as SetMovieTrailerAction:
        state.trailer = action.movieTrailer
    default:
        break
    }
    return state
}
