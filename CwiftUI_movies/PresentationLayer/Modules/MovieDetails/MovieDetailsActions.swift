//
//  MoviesActions.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 19.12.2020.
//

import SwiftUI

// MARK: - Obtain Actions
struct ObtainMovieDetailsAction: Action {
    let service = MoviesDetailsServiceImplementation()
    init(with movieId: Int, language: String = "ru-Ru") {
        service.fetchMovieDetails(movieId: movieId, language: language)
    }
}

struct ObtainMovieCastsAction: Action {
    let service = MoviesDetailsServiceImplementation()
    init(with movieId: Int, language: String = "ru-Ru") {
        service.fetchCast(movieId: movieId, language: language)
    }
}

struct ObtainMovieTrailerAction: Action {
    let service = MoviesDetailsServiceImplementation()
    init(with movieId: Int, language: String = "ru-Ru") {
        service.fetchTrailer(movieId: movieId, language: language)
    }
}

// MARK: - Set Actions
struct SetMovieDetailsAction: Action {
    let movieDetails: MovieDetails
    init(movieDetails: MovieDetails) {
        self.movieDetails = movieDetails
    }
}

struct SetMovieCastsAction: Action {
    let movieCasts: Casts
    init(movieCasts: Casts) {
        self.movieCasts = movieCasts
    }
}

struct SetMovieTrailerAction: Action {
    let movieTrailer: Trailer
    init(movieTrailer: Trailer) {
        self.movieTrailer = movieTrailer
    }
}

