//
//  Store.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import SwiftUI

class MoviesStore: ObservableObject {
    var reducer: MoviesReducer
    @Published private (set) var state: MoviesState
    
    init(reducer: @escaping MoviesReducer, state: MoviesState = MoviesState()) {
        self.reducer = reducer
        self.state = state
    }
    
    func dispatch(action: Action) {
        DispatchQueue.main.async {
            self.state = self.reducer(self.state, action)
        }
    }
}
