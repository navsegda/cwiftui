//
//  Actions.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import SwiftUI
import CFoundation

struct ObtainFilmsAction: Action {
    let service = MoviesListesServiceImplementation()
    init(with type: MoviesType, language: String = "ru-Ru", page: Int) {
        switch type {
        case .nowPlaying:
            service.fetchNowPlaying(language: language, page: page)
        case .popular:
            service.fetchPopular(language: language, page: page)
        case .topRated:
            service.fetchTopRated(language: language, page: page)
        case .upcoming:
            return
        }
    }
}

struct SetMoviesAction: Action {
    @ObservedObject var loadMoviesList = LoadMoviesList()
    let movies: [Movie]
    init(movies: [Movie]) {
        self.movies = movies
    }
}
