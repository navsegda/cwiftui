//
//  Reducer.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import SwiftUI

typealias MoviesReducer = (MoviesState, Action) -> MoviesState

func reducer(state: MoviesState, action: Action) -> MoviesState {
    var state = state
    switch action {
    case let action as SetMoviesAction:
        state.moviesItems = action.movies
    default:
        break
    }
    return state
}
