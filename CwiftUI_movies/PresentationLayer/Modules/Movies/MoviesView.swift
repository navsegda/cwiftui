//
//  MoviesView.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 22.11.2020.
//

import SwiftUI
import CArch
import struct Kingfisher.KFImage

struct MoviesView: View {
    
    // MARK: - Property wrappers
    @EnvironmentObject var store: MoviesStore
    @Binding var show: Bool
    @Binding var selected: Movie
    
    // MARK: - Public properties
    var text: String
    var animation: Namespace.ID
    
    // MARK: - Private properties
    private let columns = Array(repeating: GridItem(.flexible(), spacing: 20), count: 1)
    
    // MARK: - Body
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(store.state.moviesItems) { item in
                        ZStack(alignment: .bottomLeading) {
                            VStack {
                                KFImage(URL(string: "https://image.tmdb.org/t/p/w1280" + item.posterPath!), options: [.transition(.fade(1))])
                                    .resizable()
                                    .placeholder{ ImageLoading() }
                                    .cancelOnDisappear(true)
                                    .clipped()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(height: 500)
                            }
                            HStack {
                                VStack(alignment: .leading, spacing: 5) {
                                    Text(item.title)
                                        .bold()
                                        .font(.system(.title))
                                        .foregroundColor(Color("tint"))
                                    Text(item.releaseDate)
                                        .font(.system(.headline))
                                        .foregroundColor(.secondaryLabel)
                                }
                                Spacer()
                            }
                            .padding()
                            .background(Color(.systemBackground))
                        }
                        .cornerRadius(15)
                        .onTapGesture {
                            withAnimation(.spring()){
                                selected = item
                                show.toggle()
                                GlobalStore.detailsStore.dispatch(action: ObtainMovieDetailsAction(with: selected.id))
                                GlobalStore.detailsStore.dispatch(action: ObtainMovieCastsAction(with: selected.id, language: "ru-Ru"))
                                GlobalStore.detailsStore.dispatch(action: ObtainMovieTrailerAction(with: selected.id, language: "en-Us"))
                            }
                        }
                    }
                    .padding(.top)
                    .padding(.leading, 20)
                    .padding(.trailing, 20)
                    .shadow(color: Color.gray.opacity(0.5), radius: 12, x: 0, y: 0.0)
                }
                .navigationTitle(text)
            }
        }
    }
}
