//
//  MovDetails.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 09.01.2021.
//

import Foundation

struct BelongsToCollection: Codable {
    let id: Int
    let name, posterPath, backdropPath: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
    }
}
