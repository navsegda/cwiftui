//
//  Genre.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation

struct Genre: Codable, Identifiable {
    let id: Int
    let name: String
}
