//
//  ProductionCountries.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation

struct ProductionCountry: Codable {
    let iso3166_1, name: String

    enum CodingKeys: String, CodingKey {
        case iso3166_1 = "iso_3166_1"
        case name
    }
}
