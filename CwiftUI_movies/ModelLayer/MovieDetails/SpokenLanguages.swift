//
//  SpokenLanguages.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation

struct SpokenLanguage: Codable {
    let englishName, iso639_1, name: String

    enum CodingKeys: String, CodingKey {
        case englishName = "english_name"
        case iso639_1 = "iso_639_1"
        case name
    }
}
