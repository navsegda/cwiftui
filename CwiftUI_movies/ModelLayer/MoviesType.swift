//
//  MoviesType.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation

enum MoviesType: String {

    case nowPlaying = "NowPlaying"
    case popular = "Popular"
    case topRated = "TopRated"
    case upcoming = "Upcoming"

    var localized: String {
        self.rawValue.localized
    }

    var icon: UIImage {
        switch self {
        case .nowPlaying:
            return UIImage(systemName: "tv")!
        case .popular:
            return UIImage(systemName: "heart.fill")!
        case .topRated:
            return UIImage(systemName: "star")!
        case .upcoming:
            return UIImage(systemName: "calendar")!
        }
    }
}
