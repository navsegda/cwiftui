//
//  Movies.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 21.11.2020.
//

import CFoundation

struct APIResults: Decodable {
    let page: Int
    let numResults: Int
    let numPages: Int
    
    private enum CodingKeys: String, CodingKey {
        case page, numResults = "total_results", numPages = "total_pages"
    }
}

struct MovieAPIResults: Decodable {
    let movies: [Movie]
    
    private enum CodingKeys: String, CodingKey {
           case movies = "results"
    }
}

