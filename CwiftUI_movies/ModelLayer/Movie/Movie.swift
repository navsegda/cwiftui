//
//  Movie.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation
import Foundation

struct Movie: Decodable, Identifiable, Equatable {
    let id: Int
    let title: String
    let overview: String
    let releaseDate: String
    let posterPath : String?
    
    private enum CodingKeys: String, CodingKey {
        case id, title, overview, releaseDate = "release_date", posterPath = "poster_path"
    }
}
