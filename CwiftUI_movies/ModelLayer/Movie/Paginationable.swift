//
//  Paginationable.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation

protocol Paginationabel {

    associatedtype Results: Response

    var page: Int { get }
    var totalPages: Int { get }
    var results: Results { get }
    var totalResults: Int { get }
}
