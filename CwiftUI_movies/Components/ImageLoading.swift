//
//  ImageLoading.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 07.01.2021.
//
import SwiftUI
import struct Kingfisher.KFImage

struct ImageLoading: View {
    var body: some View {
        Rectangle()
            .fill(Color.blue)
            .opacity(0.5)
            .mask(
                Rectangle()
                    .rotationEffect(.init(degrees: 20))
            )
    }
}

struct ImageLoading_Previews: PreviewProvider {
    static var previews: some View {
        ImageLoading()
    }
}
