//
//  CastsCard.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 10.01.2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct CastsCard: View {
    var profilePicture: String
    var castName: String
    var body: some View {
        VStack(alignment: .center){
            KFImage(URL(string: "https://image.tmdb.org/t/p/w1280" + (profilePicture)))
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 100, height: 100, alignment: .center)
                .cornerRadius(20)
                .clipped()
            Text(castName)
                .multilineTextAlignment(.center)                
        }
        .frame(width: 100)
        .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
            print(castName)
        })
    }
}

struct CastsCard_Previews: PreviewProvider {
    static var previews: some View {
        CastsCard(profilePicture: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castName: "Gas Gusosus")
    }
}
