//
//  CastsView.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 10.01.2021.
//

import SwiftUI

struct CastsView: View {
    let columns = Array(repeating: GridItem(.flexible(), spacing: 10), count: 1)
    var casts: [Cast]
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .top, spacing: 10) {
                ForEach(self.casts.prefix(5)) { cast in
                    if let profilePicture = cast.profilePath {
                        CastsCard(profilePicture: profilePicture, castName: cast.name)
                    }
                }
            }
        }
    }
}

struct CastsView_Previews: PreviewProvider {
    static var previews: some View {
        CastsView(casts: testModel.cast)
    }
}
var testModel: Casts = Casts(id: 464052, cast: [Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal GadosGadosGados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: ""),Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal Gados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: ""),Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal Gados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: ""),Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal Gados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: ""),Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal Gados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: ""),Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal Gados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: ""),Cast(adult: false, gender: 1, id: 90633, knownForDepartment: "Acting", name: "Gal Gados", originalName: "Gal Gados", popularity: 1, profilePath: "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg", castID: 1, character: "", creditID: "", order: 1, department: "", job: "")], crew: [])
