//
//  StarView.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 21.12.2020.
//

import SwiftUI

struct StarView: View {
    var color: Color
    var raiting: Double
    var body: some View {
        HStack(spacing: 5){
            ForEach(1...ratingCount(raiting: raiting), id: \.self){_ in
                Image(systemName: "star.fill")
                    .foregroundColor(color)
            }
        }
    }
    
    func ratingCount(raiting: Double) -> Int {
        if raiting <= 2 {
            return 1
        } else if raiting <= 4 {
            return 2
        } else if raiting <= 6 {
            return 3
        } else if raiting <= 8 {
            return 4
        }
        return 5
    }
}

struct StarView_Previews: PreviewProvider {
    static var previews: some View {
        StarView(color: .black, raiting: 1.2)
    }
}
