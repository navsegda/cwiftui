//
//  MoviesEndpoint.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 16.12.2020.
//

import Moya
import CFoundation

enum MovieService {
    
    case nowPlaying(language: String, page: Int)
    case popular(language: String, page: Int)
    case topRated(language: String, page: Int)
    case movieDetails(movie: Int, language: String)
    case movieCast(movie: Int, language: String)
    case movieTrailer(movie: Int, language: String)
}

extension MovieService: DefaultTargetType {

    var method: Moya.Method {
        switch self {
        case .nowPlaying:
            return .get
        case .popular:
            return .get
        case .topRated:
            return .get
        case .movieDetails:
            return .get
        case .movieCast:
            return .get
        case .movieTrailer:
            return .get
        }
    }
    
    var parameters: [String : Any] {
        return [:]
    }
    
    var task: Task {
        var params: [String: Any] = [:]
        params["api_key"] = HTTPConfigurations.apiKey
        switch self {
        case .nowPlaying(language: let language, page: let page):
            params["language"] = language
            params["page"] = page
        case .popular(language: let language, page: let page):
            params["language"] = language
            params["page"] = page
        case .topRated(language: let language, page: let page):
            params["language"] = language
            params["page"] = page
        case .movieDetails(movie: _, language: let language):
            params["language"] = language
        case.movieCast(movie: _, language: let language):
            params["language"] = language
        case .movieTrailer(movie: _, language: let language):
            params["language"] = language
        }
        let encoding: ParameterEncoding = {
            switch encodingType {
            case .body:
                return JSONEncoding()
            case .url:
                return URLEncoding()
            }
        }()
        return .requestParameters(parameters: params, encoding: encoding)
    }
    
    var path: String {
        switch self {
        case .nowPlaying:
            return HTTPConfigurations.nowPlaying
        case .popular:
            return HTTPConfigurations.popularMovie
        case .topRated:
            return HTTPConfigurations.topRatedMovie
        case .movieDetails(let movieId, _):
            return "\(HTTPConfigurations.movie)\(movieId)"
        case .movieCast(movie: let movieId, language: _):
            return "\(HTTPConfigurations.movie)\(movieId)\(HTTPConfigurations.credits)"
        case .movieTrailer(movie: let movieId, language: _):
            return "\(HTTPConfigurations.movie)\(movieId)\(HTTPConfigurations.trailer)"
        }
    }
}
