//
//  MoviesService.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import SwiftUI
import Combine
import Moya

// MARK: - Public
protocol MoviesListesService {

    func fetchNowPlaying(language: String, page: Int)
    func fetchPopular(language: String, page: Int)
    func fetchTopRated(language: String, page: Int)
}

// MARK: - Private
 final class MoviesListesServiceImplementation: MoviesListesService {
    
    let loadMoviesList = LoadMoviesList()
    func fetchNowPlaying(language: String, page: Int) {
        loadMoviesList.fetchMovieList(with: .nowPlaying(language: language, page: page))
    }
    func fetchPopular(language: String, page: Int) {
        loadMoviesList.fetchMovieList(with: .popular(language: language, page: page))
    }
    func fetchTopRated(language: String, page: Int) {
        loadMoviesList.fetchMovieList(with: .topRated(language: language, page: page))
    }

}

class LoadMoviesList: ObservableObject {
    
    let provider = MoyaProvider<MovieService>(plugins: [NetworkLoggerPlugin()])
    func fetchMovieList(with movieService: MovieService){
        provider.request(movieService) { result in
            switch result {
            case let .success(moyaResponse):
                do{
                    let results = try JSONDecoder().decode(MovieAPIResults.self, from: moyaResponse.data)
                    GlobalStore.store.dispatch(action: SetMoviesAction(movies: results.movies))
                }catch let err{
                    print(err)
                }
            case let .failure(moyaError):
                print("There's an error, \(moyaError)")
            }
        }
    }

}
