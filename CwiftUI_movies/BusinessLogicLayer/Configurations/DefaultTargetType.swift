//
//  MovieDetailsService.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 08.12.2020.
//

import CFoundation
import Moya

protocol DefaultTargetType: TargetType {
    
    /// API Version
    var version: String { get }

    /// Request parameters
    var parameters: [String: Any] { get }
    
    
    var encodingType: EncodingType { get }
}


enum EncodingType {
    case body
    case url
}

extension DefaultTargetType {
    
    var version: String {
        return HTTPConfigurations.version
    }
    
    var encodingType: EncodingType {
        return .url
    }
}

extension DefaultTargetType {
    var baseURL: URL {
        return URL(string: "\(HTTPConfigurations.baseURL)/\(version)")!
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    
}
