//
//  HttpConfigurations.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 07.01.2021.
//

import Foundation

struct HTTPConfigurations {
    //MARK : - API Key
    static let baseURL = "https://api.themoviedb.org"
    static let apiKey = "2f2c2b67478cef3d33e9b0b4bc1a892c"
    static let version = "3"
    
    //MARK : - URL Request
    static let movie = "/movie/"
    static let popularMovie = "movie/popular"
    static let upcomingMovie = "movie/upcoming"
    static let topRatedMovie = "movie/top_rated"
    static let nowPlaying = "movie/now_playing"
    static let reviews = "/reviews"
    static let recomendation = "/recommendations"
    static let credits = "/credits"
    static let trailer = "/videos"
    
    enum languages: String {
        case russian = "ru-Ru"
        case english = ""
    }
}
