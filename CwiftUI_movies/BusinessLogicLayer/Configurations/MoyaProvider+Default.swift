//
//  MoyaProvider+Default.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 16.12.2020.
//

import Moya
import Foundation

extension MoyaProvider {
    
    static func `default`() -> MoyaProvider {
        var plugins: [PluginType] = []
        
        #if DEBUG
        let network = NetworkLoggerPlugin()
        plugins.append(network)
        #endif

        return MoyaProvider(plugins: plugins)
    }
}
