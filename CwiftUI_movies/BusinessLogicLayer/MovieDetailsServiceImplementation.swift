//
//  MovieDetailsServiceImplementation.swift
//  CwiftUI_movies
//
//  Created by Игорь Лемешевски on 19.12.2020.
//

import SwiftUI
import Combine
import Moya

protocol MovieDetailsService {
    
    func fetchMovieDetails(movieId: Int, language: String)
    func fetchCast(movieId: Int, language: String)
    func fetchTrailer(movieId: Int, language: String)
}

final class MoviesDetailsServiceImplementation: MovieDetailsService {
    
    let movieDetails = FetchMovieDetails()
    func fetchMovieDetails(movieId: Int, language: String = "ru-Ru") {
        movieDetails.fetchMovieDetails(with: .movieDetails(movie: movieId, language: language))
    }
    
    func fetchCast(movieId: Int, language: String) {
        movieDetails.fetchCasts(with: .movieCast(movie: movieId, language: language))
    }
    
    func fetchTrailer(movieId: Int, language: String) {
        movieDetails.fetchTrailer(with: .movieTrailer(movie: movieId, language: language))
    }
    
}

class FetchMovieDetails: ObservableObject {
    
    let provider = MoyaProvider<MovieService>(plugins: [NetworkLoggerPlugin()])
    @Published var details: MovieDetails?
    func fetchMovieDetails(with movieService: MovieService){
        provider.request(movieService) { result in
            switch result {
            case let .success(moyaResponse):
                do{
                    let results = try JSONDecoder().decode(MovieDetails.self, from: moyaResponse.data)
                    GlobalStore.detailsStore.dispatch(action: SetMovieDetailsAction(movieDetails: results))
                }catch let err{
                    print(err)
                }

            case let .failure(moyaError):
                print("There's an error, \(moyaError)")
            }
        }
    }
    
    func fetchCasts(with movieService: MovieService) {
        provider.request(movieService) { result in
            switch result {
            case let .success(moyaResponse):
                do{
                    let results = try JSONDecoder().decode(Casts.self, from: moyaResponse.data)
                    GlobalStore.detailsStore.dispatch(action: SetMovieCastsAction(movieCasts: results))
                }catch let err{
                    print(err)
                }
            case let .failure(moyaError):
                print("There's an error, \(moyaError)")
            }
        }
    }
    
    func fetchTrailer(with movieService: MovieService) {
        
        provider.request(movieService) { result in
            switch result {
            case let .success(moyaResponse):
                do{
                    let results = try JSONDecoder().decode(Trailer.self, from: moyaResponse.data)
                    GlobalStore.detailsStore.dispatch(action: SetMovieTrailerAction(movieTrailer: results))
                }catch let err{
                    print(err)
                }
            case let .failure(moyaError):
                print("There's an error, \(moyaError)")
            }
        }
    }
    
}

